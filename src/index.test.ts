import { Either, left, right } from '.';

const toBeLeft = <A, B>(received: Either<A, B>, value: A) => {
  if (received.isLeft && received.value === value) {
    return {
      message: () => `expected ${received} not to be a Left with value ${value}`,
      pass: true,
    };
  }
  return {
    message: () => `expected ${received} to be a Left with value ${value}`,
    pass: false,
  };
};

const toBeRight = <A, B>(received: Either<A, B>, value: B) => {
  if (received.isRight && received.value === value) {
    return {
      message: () => `expected ${received} not to be a Right with value ${value}`,
      pass: true,
    };
  }
  return {
    message: () => `expected ${received} to be a Right with value ${value}`,
    pass: false,
  };
};

interface CustomMatchers<R = unknown> {
  toBeLeft<A>(value: A): R;
  toBeRight<B>(value: B): R;
}

declare global {
  namespace jest {
    interface Expect extends CustomMatchers {}
    interface Matchers<R> extends CustomMatchers<R> {}
    interface InverseAsymmetricMatchers extends CustomMatchers {}
  }
}

expect.extend({ toBeLeft, toBeRight });

describe('Either', () => {
  describe('Left', () => {
    it('value is equal to the value', () => {
      expect(left(1).value).toEqual(1);
      expect(left("abc").value).toEqual("abc");
      const array: unknown[] = [];
      expect(left(array).value).toEqual([]);
      expect(left(array).value).toBe(array);
      const obj = Object.freeze({});
      expect(left(obj).value).toBe(obj);
    });

    it('isLeft is true', () => {
      expect(left('Alice').isLeft).toEqual(true);
    });

    it('isRight is false', () => {
      expect(left('Alice').isRight).toEqual(false);
    });

    describe('getOrElse', () => {
      it('returns the given value', () => {
        const either: Either<string, string> = left('error');
        expect(either.getOrElse('fallback')).toEqual('fallback');
      });
    });

    describe('getOrElseLazy', () => {
      it('returns the result of the given function', () => {
        const ifLeft = jest.fn().mockReturnValue('fallback');
        const either: Either<string, string> = left('error');
        expect(either.getOrElseLazy(ifLeft)).toEqual('fallback');
        expect(ifLeft).toHaveBeenCalledTimes(1);
        expect(ifLeft).toHaveBeenCalledWith('error');
      });
    });

    describe('getOrElseThrow', () => {
      it('throws the value when no function is provided', () => {
        const either: Either<string, string> = left('error');
        const f = () => either.getOrElseThrow();
        expect(f).toThrow('error');
        expect(f).not.toThrow(Error);
      });

      it('throws the result of the the given function', () => {
        const either: Either<string, string> = left('error');
        const f = () => either.getOrElseThrow(s => new Error(s));
        expect(f).toThrow('error');
        expect(f).toThrow(Error);
      });
    });

    describe('swap', () => {
      it('swaps to a Right', () => {
        const swapped = left('Alice').swap();
        expect(swapped).toBeRight('Alice');
      });
    });

    describe('map', () => {
      it('returns this without invoking the function', () => {
        const f = jest.fn();
        const either: Either<string, never> = left('Alice');
        const result = either.map(f);
        expect(result).toBe(either);
        expect(f).not.toHaveBeenCalled();
      });
    });

    describe('flatMap', () => {
      it('returns this without invoking the function', () => {
        const f = jest.fn();
        const either: Either<string, never> = left('Alice');
        const result = either.flatMap(f);
        expect(result).toBe(either);
        expect(f).not.toHaveBeenCalled();
      });
    });

    describe('fold', () => {
      it('applies the given function to the value', () => {
        const fa = jest
          .fn<number, [string]>()
          .mockReturnValue(1);
        const fb = jest
          .fn<number, [string]>()
          .mockReturnValue(2);

        const either: Either<string, never> = left('Alice');
        const result = either.fold<number>(fa, fb);
        expect(result).toEqual(1);
        expect(fa).toHaveBeenCalledTimes(1);
        expect(fa).toHaveBeenCalledWith('Alice');
        expect(fb).not.toHaveBeenCalled();
      });
    });
  });

  describe('Right', () => {
    it('value is equal to the value', () => {
      expect(right(1).value).toEqual(1);
      expect(right("abc").value).toEqual("abc");
      const array: unknown[] = [];
      expect(right(array).value).toEqual([]);
      expect(right(array).value).toBe(array);
      const obj = Object.freeze({});
      expect(right(obj).value).toBe(obj);
    });

    it('isLeft is false', () => {
      expect(right('Alice').isLeft).toEqual(false);
    });

    it('isRight is true', () => {
      expect(right('Alice').isRight).toEqual(true);
    });

    describe('swap', () => {
      it('swaps to a Left', () => {
        const swapped = right('Alice').swap();
        expect(swapped).toBeLeft('Alice');
      });
    });

    describe('map', () => {
      it('returns a Right containing the application of f', () => {
        const f = jest.fn<number, [string]>().mockReturnValue(123);
        const either: Either<never, string> = right('Alice');
        const result = either.map(f);
        expect(result).toBeRight(123);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith('Alice');
      });
    });

    describe('flatMap', () => {
      it('returns the application of f', () => {
        const f = jest
          .fn<Either<never, number>, [string]>()
          .mockReturnValue(right(123));

        const either: Either<never, string> = right('Alice');
        const result = either.flatMap(f);
        expect(result).toBeRight(123);
        expect(f).toHaveBeenCalledTimes(1);
        expect(f).toHaveBeenCalledWith('Alice');
      });
    });

    describe('fold', () => {
      it('applies the given function to the value', () => {
        const fa = jest
          .fn<number, [string]>()
          .mockReturnValue(1);
        const fb = jest
          .fn<number, [string]>()
          .mockReturnValue(2);

        const either: Either<never, string> = right('Alice');
        const result = either.fold<number>(fa, fb);
        expect(result).toEqual(2);
        expect(fb).toHaveBeenCalledTimes(1);
        expect(fb).toHaveBeenCalledWith('Alice');
        expect(fa).not.toHaveBeenCalled();
      });
    });
  });
});
