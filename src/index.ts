/**
 * Represents values that can be one of two possible types.
 * Instances of `Either` are either a `Left` or a `Right`.
 */
interface IEither<out A, out B> {

  /**
   * Get either the value of `Left` or `Right`.
   */
  value: A | B;

  /**
   * Get either the value of `Right` or return the given value for `Left`.
   * @param ifLeft Fallback value returned for `Left`.
   * @returns Either the value of `Right` or `ifLeft`.
   */
  getOrElse(ifLeft: B): B;

  /**
   * Get either the value of `Right` or return the result of the given function.
   * @param ifLeft Function applied to the value if `Left`.
   * @returns Either the value of `Right` or the result of `isLeft`.
   */
  getOrElseLazy(ifLeft: (a: A) => B): B;

  /**
   * Get either the value of `Right` or throw.
   * If a function is provided, then the return value is thrown.
   * Otherwise, the value of `Left` is thrown instead.
   * @param f Function applied to the value if `Left`.
   * @returns Either the value of `Right` or throws.
   */
  getOrElseThrow<C>(f?: (a: A) => C): B;

  /**
   * `true` when this is a `Left`.
   */
  isLeft: boolean;

  /**
   * `true` when this is a `Right`.
   */
  isRight: boolean;

  /**
   * Apply `f` if this is a `Right` to make a new `Either` with the result.
   * @param f Function to apply if this is a `Right`.
   * @returns `Right` containing the application of `f` or `Left`.
   */
  map<C>(f: (b: B) => C): Either<A, C>;

  /**
   * Apply `f` if this is a `Right`.
   * @param f Function to apply if this is a `Right`.
   * @returns The application of `f` or `Left`.
   */
  flatMap<C>(f: (b: B) => Either<A, C>): Either<A, C>;

  /**
   * Apply `fa` if this is a `Left` or `fb` if this is a `Right`.
   * @param fa Function to apply if this is a `Left`.
   * @param fb Function to apply if this is a `Right`.
   * @returns The application of `fa` or `fb`.
   */
  fold<C>(fa: (a: A) => C, fb: (b: B) => C): C;

  /**
   * Swap this `Either` so that a `Left` becomes a `Right` or vice versa.
   */
  swap(): Either<B, A>;
}


interface Left<out A, out B = never> extends IEither<A, B> {
  value: A;
  isLeft: true;
  isRight: false;
}

interface Right<out B, out A = never> extends IEither<A, B> {
  value: B;
  isLeft: false;
  isRight: true;
}

class LeftImpl<A, B = never> implements IEither<A, B> {
  readonly isLeft = true;
  readonly isRight = false;

  constructor(readonly value: A) {}

  getOrElse = (ifLeft: B): B => ifLeft;

  getOrElseLazy = (ifLeft: (a: A) => B): B => ifLeft(this.value);

  getOrElseThrow = <C>(f?: (a: A) => C): B => {
    throw f ? f(this.value) : this.value;
  }

  map = <C>(f: (b: B) => C): Either<A, C> => this as any;

  flatMap = <C>(f: (b: B) => Either<A, C>): Either<A, C> => this as any;

  fold = <C>(fa: (a: A) => C, fb: (b: B) => C): C => fa(this.value);

  swap = (): Either<B, A> => new RightImpl<A, B>(this.value);
}

class RightImpl<B, A = never> implements IEither<A, B> {
  readonly isLeft = false;
  readonly isRight = true;

  constructor(readonly value: B) {}

  getOrElse = (ifLeft: B) => this.value;

  getOrElseLazy = (ifLeft: (a: A) => B) => this.value;

  getOrElseThrow = <C>(f?: (a: A) => C): B => this.value;

  map = <C>(f: (b: B) => C): Either<A, C> => new RightImpl(f(this.value));

  flatMap = <C>(f: (b: B) => Either<A, C>): Either<A, C> => f(this.value);

  fold = <C>(fa: (a: A) => C, fb: (b: B) => C): C => fb(this.value);

  swap = (): Either<B, A> => new LeftImpl(this.value);
}

/**
 * Represents values that can be one of two possible types.

 * Instances of `Either` are either a `Left` or a `Right`.
 */
export type Either<A, B> = Left<A, B> | Right<B, A>;

/**
 * Create a `Left` with the given underlying value.
 * @param a Underlying value.
 * @returns `Left(a)`.
 */
export const left = <A, B = never>(a: A): Left<A, B> => new LeftImpl(a);

/**
 * Create a `Right` with the given underlying value.
 * @param b Underlying value.
 * @returns `Right(b)`.
 */
export const right = <B, A = never>(b: B): Right<B, A> => new RightImpl(b);
